//var value = 1;
//var value = prompt('Opcion 1 o 2?')
function result (value){
switch (value){
    case 1:
        console.log('I will always run')
        break;
    case 2:
        console.log('I will never run')
        break;
}
}

// El switch case realiza la funcion de dar opciones o como 
// menu, para que al seleccionar la opcion deceada muestre
// lo que se encuentra en dicha opcion

// Resultado
// result(1)
// JScript23.js:6 I will always run
// undefined
// result(2)
// JScript23.js:9 I will never run
// undefined