class Padre{
    tarea(){
        console.log('Tarea del Padre..');
    }

    tarea(parametro){
        console.log('Tarea del padre con valor [Sobrecarga]: ');
    }
}

(new Padre()).tarea('xyz');
(new Padre()).tarea();

class Hijo extends Padre{

    //Sobrecarga
    tarea(){
        super.tarea();
        console.log(' + Tarea del hijo.. ');
    }
}

(new Hijo()).tarea();