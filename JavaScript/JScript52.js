class Animal {
    constructor(){
        console.warn('Ha nacido un pato. ');
    }
        hablar(){
         return 'cuak';
    }
}

var pato = new Animal();
pato.hablar();

var donald = new Animal();
donald.hablar();


class Clase{
    
    constructor(){
        this.name = 'Luis';
        console.log('Constructor: ' + this.name);
    }

    metodo(){
        console.log('Metodo: ' + this.name);
    }
}

var c = new Clase();

//console.log(c.name)
c.name;
c.metodo();