function John(){
    return 'John';
}
function Jacob(){
    return 'Jacob';
}
function result(name){
switch (name){
    case John():
    console.log('I will run if name == "John"');
    break;

    case 'Ja' + 'ne':
            console.log('I will run if name == "Jane"');
            break;
    case John() + '' + Jacob() + 'Jingleheimner Schmidt':
    console.log('His name is equal to name too!')
    break;
}
}


//Resultado
//Al mandar a traer la funcion result(case) da como resultad lo siguiente:
// result(John())
// JScript25.js:10 I will run if name == "John"
// undefined
// result('Ja' + 'ne')
// JScript25.js:14 I will run if name == "Jane"
// undefined
// result('Jane')
// JScript25.js:14 I will run if name == "Jane"
// undefined
// result(John() + '' + Jacob() + 'Jingleheimner Schmidt')
// JScript25.js:17 His name is equal to name too!