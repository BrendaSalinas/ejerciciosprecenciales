var animal = 'Lion';

function result (animal){
switch (animal){
    case 'Dog' :
        console.log('I will not run sincen animal !== "Dog"'); //result('Dog')
        break;

        case 'Cat' :
                console.log('I will not run sincen animal !== "Cat"'); //result('Cat')
                break;

                default :
                        console.log('I will not run sincen animal does not any other case'); //result('Lion')
}
}

// El switch case realiza la funcion de dar opciones o como 
// menu, para que al seleccionar la opcion deceada muestre
// lo que se encuentra en dicha opcion.

// Resultado
// result('Dog')
// JScript24.js:6 I will not run sincen animal !== "Dog"
// undefined
// result('Cat')
// JScript24.js:10 I will not run sincen animal !== "Cat"
// undefined
// result()
// JScript24.js:14 I will not run sincen animal does not any other case
// undefined