var age = 20;
var isLegal = age >= 18;
console.log(isLegal)
//Le accine un valor a la variable age
//para que pueda realizarse la comparacion.
//En este caso el resultado seria true
var height = 15;
var tall = height >= 5.11;
console.log(height)
//Le accine un valor a la variable age
//para que pueda realizarse la comparacion.
//En este caso el resultado seria 15
var suitable = isLegal && tall;
console.log(suitable)
//El resultado es true
var isRoyalty = status === 'royalty';
console.log(isRoyalty)
//El resultado es false
var hasInvitation = 5;
var specialCase = isRoyalty && hasInvitation;
console.log(specialCase)
//Le accine un valor a la variable age
//para que pueda realizarse la comparacion.
//En este caso el resultado seria falso
var canEnterOurBar = suitable || specialCase;
console.log(canEnterOurBar)
//El resultado es true