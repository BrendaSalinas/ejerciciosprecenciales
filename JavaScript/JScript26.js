var x = "c"

function result (x){
switch (x){
    case "a" :
    case "b" :
    case "c" :
        console.log('Either a, b, or c was selected.');
        break ;
    case "d" :
        console.log('Only d was selected.');
        break;
    default :
        console.log('No case was mactched.');
        break ;
}
}

//El swich case funciona como menu de opciones
//Al colocar en consola result('Opcion de case') manda a traer 
//lo que contenda dicha opcion.

// Resultado
// result('a')
// JScript26.js:8 Either a, b, or c was selected.
// undefined
// result('b')
// JScript26.js:8 Either a, b, or c was selected.
// undefined
// result('c')
// JScript26.js:8 Either a, b, or c was selected.
// undefined
// result('d')
// JScript26.js:11 Only d was selected.
// undefined
// result()
// JScript26.js:14 No case was mactched.
// undefined