class MiMensaje extends HTMLElement{
    constructor(){
        super();
        this.addEventListener('click', function(e){
            alert("Click en Mensaje");
        });
        console.log('constructor: Cuando el elemento es creado');
    }

    static get observedAttributes (){
        return ['msj', 'casi-visible'];
    }

    connectedCallback(){
        console.log('connectedCallback: Cuando el elemento es insertado en el documento')
    }

    disconnectedCallback(){
        alert('disconnected: Cuando el elemento es eliminado del documento')
    }

    adoptedCallback(){
        alert('adoptedCallback: Cuando el elemento es adoptado del documento')
    }

    attributeChangedCallback(attrName, oldVal, newVal){
        console.log('attributeChangedCallback: Cuando cambia un attributo')

        if(attrName==='msj'){
            this.pintaMensaje(newVal);
        }
        if(attrName==='casi-visible'){
            this.setCasiVisible();
        }
    }

    pintaMensaje(msj){
        this.innerHTML = msj
    }

    
    get msj (){
       return this.getAttribute('msj')
    }

    set msj (val){
        this.setAttribute('msj', val)
    }

    get casiVisible (){
        return this.hasAttribute('casi-visible');
    }

    set casiVisible(value){
        if(value){
            this.setAttribute('casi-visible', '');
        }else{
            this.removeAttribute('casi-visible', '');
        }
    }

    setCasiVisible(){
        if(this.casiVisible){
            this.style.opacity=0.1;
        }else{
            this.style.opacity=1;
        }
    }
}

customElements.define('mi-mensaje', MiMensaje);


let miMensaje2 = document.createElement('mi-mensaje')
miMensaje2.msj = 'Otro mensaje';
document.body.appendChild(miMensaje2);

let miMensaje3 = new MiMensaje();
miMensaje3.msj = 'Tercer mensaje';
document.body.appendChild(miMensaje3);