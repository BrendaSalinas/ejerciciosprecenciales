//Ejercicio 1
//Opcion 1: Bucle con let
console.log('Antes: ', p)

for(let p=0; p<3; p++)
    console.log('- ',p)

    console.log('Despues: ', p)

    //Opcion 2: Bucle con var
    console.log('Antes ', p)


for(var p=0; p<3; p++)
    console.log('- ',p)

    console.log('Despues: ', p)


    function foo(){
        if(true){
            let i = 1;
        }
        console.log(i)//Referencia de error
    }

//Ejercicio 2

const name = 'ValorConstante'
console.log(name)
//name ='CambioValor'
//Error por que es constante

//var = Globales
//let = Locales
//const = Definida



//Ejercicio 3
const user ={name: 'Juan'}
user.name = 'Luis'
console.log(user.name)

//Ejercicio 4
var num = NaN

num ==NaN
Number.isNaN(num)

typeof(num)


//Ejercicio 5

var array = ['a', 'b', 'c']

array.push('d')
// Resultado
// 4
// console.log(array)
// VM580:1 
// (4) ["a", "b", "c", "d"]
// 0: "a"
// 1: "b"
// 2: "c"
// 3: "d"
// length: 4
// __proto__: Array(0)

array.pop()
// Resultado
// "c"
// console.log(array)
// VM615:1 
// (2) ["a", "b"]
// 0: "a"
// 1: "b"
// length: 2
// __proto__: Array(0)

array.unshift('Z')
// Resultado
// 4
// console.log(array)
// VM463:1 
// (4) ["Z", "a", "b", "c"]
// 0: "Z"
// 1: "a"
// 2: "b"
// 3: "c"
// length: 4
// __proto__: Array(0)

array.shift()
// Resultado
// "a"
// console.log(array)
// VM637:1 (2) ["b", "c"]
// 0: "b"
// 1: "c"
// length: 2
// __proto__: Array(0)


//Ejercicio 6

var array = [1,2,3]
array.push(4,5,6)
array.push([7,8,9])
console.log(array)
// Resultado
// (7) [1, 2, 3, 4, 5, 6, Array(3)]
// 0: 1
// 1: 2
// 2: 3
// 3: 4
// 4: 5
// 5: 6
// 6: (3) [7, 8, 9]
// length: 7
// __proto__: Array(0)


// Ejercicio 7

var array = ['a', 'b', 'c', 'd', 'e']

array.slice(2,4)

array.splice(2,2)
array.splice(1,0,'z','x')
// Resultado
// []
// length: 0
// __proto__: Array(0)


//Ejercicio 8

 var array = [1,8,2,32,9,7,4]
 array.sort()
//  Resultado
//  (7) [1, 2, 32, 4, 7, 8, 9]
// 0: 1
// 1: 2
// 2: 32
// 3: 4
// 4: 7
// 5: 8
// 6: 9
// length: 7
// __proto__: Array(0)

//Ejercicio 9

var fc = function (a,b){
    return a -b;
}

array.sort(fc)
// Resultado
// (7) [1, 2, 4, 7, 8, 9, 32]
// 0: 1
// 1: 2
// 2: 4
// 3: 7
// 4: 8
// 5: 9
// 6: 32
// length: 7
// __proto__: Array(0)

//Ejercicio 10

var arr =[2,3,9,54,6,1]
var f = function (a,b){
    if(a>b){
        return 1;
    } else if(a<b){
        return -1;
    }
    return 0;
}
console.log(arr.sort(f))
// Resultado
// (6) [1, 2, 3, 6, 9, 54]
// 0: 1
// 1: 2
// 2: 3
// 3: 6
// 4: 9
// 5: 54
// length: 6
// __proto__: Array(0)

//Ejercicio 11

