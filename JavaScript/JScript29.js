var a = 'hello' && '';
console.log(a)
//El resultado es vacio
var b = '' && [];
console.log(b)
//El resultado es vacio
var c = undefined && 0;
console.log(c)
//El resultado es undefined
var d = 1 && 5;
console.log(d)
//El resultado es 5
var e = 0 && {};
console.log(e)
//El resultado es 0
var f = 'Hi' && [] && 'Done';
console.log(f)
//El resultado es Done
var g = 'Bye' && undefined && 'Adios';
console.log(g)
//El resultado es undefined