//Ejercicio1

var clientes = ['c1', 'c2', 'c3']
var empleados = ['e1', 'e2', 'e3']

var contactos = clientes.concat(empleados)
console.log(contactos)
//Resultado
// (6) ["c1", "c2", "c3", "e1", "e2", "e3"]
// 0: "c1"
// 1: "c2"
// 2: "c3"
// 3: "e1"
// 4: "e2"
// 5: "e3"
// length: 6
// __proto__: Array(0)

// var contactos = clientes.join(empleados)
// console.log(contactos)
// Resultado
// c1e1,e2,e3c2e1,e2,e3c3

// var contactos = clientes.push(empleados)
// console.log(contactos)
//Resultado
//4

// var contactos = clientes.splice(empleados)
// console.log(contactos)
//Resultado
// (4) ["c1", "c2", "c3", Array(3)]
// 0: "c1"
// 1: "c2"
// 2: "c3"
// 3: (3) ["e1", "e2", "e3"]
// length: 4
// __proto__: Array(0)


// Ejercicio 2

var numbers = [5, 32, 43, 4];
numbers.filter(function(n) { return n % 2 !== 0; });
//Resultado
// (2) [5, 43]
// 0: 5
// 1: 43
// length: 2
// __proto__: Array(0)
//El 2 determina el numero de valores dentro 
//del arreglo se mostraean, en este caso serian 2

//Ejercicio 3
var people = [{
    id: 1,
    name: "John", age: 28
    }, {
    id: 2,
    name: "Jane", age: 31
    }, { id: 3,
    name: "Peter", age: 55 }];

      people.filter(function(persona) {
        return persona.age < 35;
       });
//Resultado
// (2) [{…}, {…}]
// 0: {id: 1, name: "John", age: 28}
// 1: {id: 2, name: "Jane", age: 31}
// length: 2
// __proto__: Array(0)

//Ejecicio 4
let people = [
    { name: "bob", id:1 }, 
    { name: "john", id:2 }, 
    { name: "alex", id:3 }, 
    { name: "john", id:3 } ];
   
    function repetir(list,value){
      let count = 0;
      list.forEach((v) => {
      if(v.name==value){
        count++;
      }
    });
    console.log(count)
    }
//     Resultado
//     repetir(people,'john')
// VM727:14 2
// undefined
// repetir(people,'alex')
// VM727:14 1
// undefined
// repetir(people,'bob')
// VM727:14 1
// undefined


    //Ejercicio 5
    function maxmin(){
    var myArray = [1, 2, 3, 4];
    var min = Math.min.apply(null,myArray)
    var max = Math.max.apply(null,myArray)

    console.log('Min: '+ min + ' Max: ' + max)
    }
    maxmin();
    //Resultado
    
    // 1 4


    //Ejercicio 6

    var object = {
      key1: 10, 
      key2: 3, 
      key3: 40, 
      key4: 20};

array = [
  object.key1, 
  object.key2, 
  object.key3, 
  object.key4]

var ordenar = function(a,b){
  return a - b;
}

array.sort(ordenar)

// Resultado
// (4) [3, 10, 20, 40]
// 0: 3
// 1: 10
// 2: 20
// 3: 40
// length: 4
// __proto__: Array(0)

