var foo = function (val) {
return val || 'default';
}

console.log( foo ('burger'))
//El resultado es burger
console.log( foo (100))
//El resultado es 100
console.log( foo ([]))
//El resultado es Array(0)
console.log( foo (0))
//El resultado es default
console.log( foo (undefined))
//El resultado es default