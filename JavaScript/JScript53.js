class Forma{
    constructor(){
        console.log('Soy una forma geometrica');
    }
        gritar(){
            console.log('Yep!!')
        }
    }


    //Clase Hija
    class Cuadrado extends Forma{
        constructor(){
            super();
                console.log('Soy un cuadrado')
        }  
            }
    
            class Circulo extends Forma{
                constructor(){
                    super();
                        console.log('Soy un circulo')
                }  
                    }
        

                    class Triangulo extends Forma{
                        constructor(){
                            super();
                                console.log('Soy un triangulo')
                        }  
                            }

        var c1 = new Cuadrado();
        //'Soy una forma geometrica'
        //'Soy un cuadrado'
        c1.gritar();
        //'Yep!!'

        var t1 = new Triangulo();
         //'Soy una forma geometrica'
        //'Soy un triangulo'
        c1.gritar();
        //'Yep!!'
                