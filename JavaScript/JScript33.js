var i = 0;
while (i<3){
    if ( i == 1 ){
        i = 2;
        continue;
    }
    console.log(i);
    i++;
}

//El While se encarga de realizar una numeracion
//que va de 0 a 2, lo que imprime en consola
//es 0,2 ya que el 1 es el numero en el que se 
//esta comparando la letra i en la funcion if

// Resultado
// 0
// 2