function procesaEvento(elemento,e){
    console.log('Evento click');
    console.log(elemento);
    console.log(e);
}

function muestraMensaje(){
    console.log('Haz realizado clock en el raton');
}

var elDiv = document.getElementById('div_principal');
elDiv.addEventListener('click', muestraMensaje,false);
//mas de una asociacion
//elDiv.addEventListener('click', muestraMensaje,true);

//desasociar la funciomn al evento
elDiv.removeEventListener('click', muestraMensaje, false);



//Otro Ejercicio

var elBoton = document.getElementById('miBoton');
var  miEvento = new Event('Alertota');
elBoton.addEventListener('Alertota', funcionEventHandler,false);

function funcionEventHandler(){
    alert("Se ejecuta el evento 'Alertota'")
}

function disparaEvento(elemento){
    elemento.dispatchEvent(miEvento);
}