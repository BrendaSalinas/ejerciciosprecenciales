//Las || representan una opcion logica o
var a = 'Hello' || '';
console.log(a)
//El resultado es Hello
var b = '' || [];
console.log(b)
//El resultado es []
var c = '' || undefined;
console.log(c)
//El resultado es undefined
var d = 1 || 5;
console.log(d)
//El resultado es 1
var e = 0 || {};
console.log(e)
//El resultado es {}
var f = 0 || '' || 5;
console.log(f)
//El resultado es 5
var g = '' || 'yay' || 'boo';
console.log(g)
//El resultado es yay