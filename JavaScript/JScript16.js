function f(x, y=2, z=7){
    return x+y+z;
}
console.log(f(5,undefined)); //Resultado

// El resultado es 14, este resuktado es dado ya que el valor 
// de y es 2, z vale 7, ala hora de imprimir se asigna el valor de x 
// que seria 5 el cual al sumar nos da como resultado el 14.

// Resultado 
// 14