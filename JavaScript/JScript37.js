var namedSum = function sum (a,b) {
    return a + b ;
}
var anonSum = function (a,b) {
    return a + b ;
}

namedSum(1,3);
anonSum(1,3);

//El resultado de ambas funciones es la suma 
//de las cantidades colocadas en el namedSum(1,3)
//o anonSum(1,3)

// Resultado
// namedSum(1,3)
// 4
// anonSum(1,3)
// 4
