function errores (){
    try{ 
        function_que_no_existe();
    } catch (ex){
        console.log('Error detectado: ' + ex.desciption);
    }
    console.log('Continua con la funcion aun despues del error')
}

//Imprime ambos textos colocados dentro de la funcion

// Resultado
// errores()
// JScript38.js:5 Error detectado: undefined
// JScript38.js:7 Continua con la funcion aun despues del error
// undefined