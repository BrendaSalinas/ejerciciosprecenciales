var a = 0;
var str = 'not a';
var b = '';
// b = a == 0 ? (a = 1, str += ' test') : (a = 2)

function result (a){
    if( a==0 ){
        a=1
        str += ' test'
        b= str
    }else{
        a=2
    }
    return b;
}


// El resultado de b es 'not a test' el cual resulta 
// del valor de str con una concatenacion de la palabra test

// Resultado
// result(a)
// "not a test"