let decirNombre = function(obj){
    obj.hablar = function(){
        console.log(this.nombre);
    };
};

const BETO ={
    nombre: 'Beto',
    edad: 22
};

const JUAN ={
    nombre: 'Juan',
    edad: 25
};

decirNombre(JUAN)
decirNombre(BETO)

JUAN.hablar();
BETO.hablar();


var contadorClick = 0;

function updateCounter(e){
    console.log(e);
    console.log(e.detail);
    console.log('contador' + contadorClick);
    contadorClick++;
    document.querySelector('#num').textContent = e.detail;
}