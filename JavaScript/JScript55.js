class Animal{
    constructor(n){
        this.nombre = n;
    }

    get nombre (){
        console.log('No puedes ver el valor');
        return 'Sr. ' + this._nombre ;
    }

    set nombre(n){
        console.log('No lo puedes modificar')
        this._nombre = n.trim();
    }

    hablar(){
        return 'cuak';
    }
    quienSoy(){
        return 'Hola, soy ' + this.nombre;
    }
}

//Creacion de objetos
var pato = new Animal('Donal');

console.log(pato.nombre);            //'Sr. Donald'
console.log(pato.nombre = 'Lucas');  // 'Lucas'
console.log(pato.nombre);             // 'Sr. Lucas'